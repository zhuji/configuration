#!/bin/bash

var=$(sudo ps x|grep 'sslocal -c .bwh.json -d start')
if [ ${#var} == 0 ]
then
    echo 'NO bwhost detected!'
    sudo sslocal -c .bwh.json -d start
else
    echo 'bwhost detected!'
fi
