Download the patched fonts. I chose DejaVuSansMono as my font since I like it most.
Install this font in Windows to make it accessible for all programs.
Open PuTTY and make changes to the settings:
Under appearance select the patched font
Select font quality Clear Type
Under Translation select character set UTF-8
Apply settings and restart the PuTTY session

see http://mschulte.nl/posts/using-powerline-in-PuTTY.html