#!/bin/bash

# 更新源后清理
sudo rm -rf /var/lib/apt/lists/*
sudo apt-get update

# 必备软件
sudo apt-get -y install git vim wget zsh htop shadowsocks proxychains ntp tree tldr cloc

# 编译软件
sudo apt-get -y install g++ build-essential linux-headers-$(uname -r)

# 安装chrome: https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
# wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
# sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
# sudo apt-get update
# sudo apt-get -y install google-chrome-stable

# 安装snap软件
# sudo snap install pycharm-professional --channel=2018.1/stable --classic
# sudo snap install clion --channel=2018.1/stable --classic
# sudo snap install redis-desktop-manager
# sudo snap install postman
# sudo snap install slack --classic

# 安装其他
sudo apt-get -y install redis python3-pip python3-distutils

# 安装python3软件包
pip3 install pandas scipy matplotlib flask itchat ipython pyzmq aiohttp tabulate tornado aioredis

# 安装oh-my-zsh
# sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
# chsh -s /bin/zsh

# 配置shadowsocks
cp /etc/shadowsocks/config.json ~/.bwh.json

# 安装公开字体
# sudo apt-get install xfonts-wqy fonts-wqy-microhei fonts-wqy-zenhei
# fc-cache
# sudo fc-cache -f -s -v
# fc-list :lang=zh

# 卸载蓝牙
# sudo apt-get purge 'bluez*'

# proxychains设置
# sudo vim /etc/proxychains.conf
# 修改最后一行socks5 127.0.0.1 1080
# cat /usr/bin/proxychains
# export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libproxychains.so.3

